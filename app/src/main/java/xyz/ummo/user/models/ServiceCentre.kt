package xyz.ummo.user.models

data class ServiceCentre(val serviceCentreName: String,
                         val serviceCentreLocation: String,
//                         val serviceCentreImageUrl: String,
                         val productName: String,
                         val productDescription: String)